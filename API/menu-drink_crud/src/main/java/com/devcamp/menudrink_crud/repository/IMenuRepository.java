package com.devcamp.menudrink_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink_crud.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu, Long>{

}
