package com.devcamp.menudrink_crud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.menudrink_crud.model.CDrink;
import com.devcamp.menudrink_crud.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/drinks")
public class CDrinkController {
	@Autowired 
	IDrinkRepository pDrinkRepository;
	
	@GetMapping("/all")
	public ResponseEntity<Object> getAllDrink(){
		try {
			List<CDrink> pDrinkLists = new ArrayList<CDrink>();
			
			pDrinkRepository.findAll().forEach(pDrinkLists::add);
			
			return new ResponseEntity<>(pDrinkLists, HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/details/{drinkId}")
    public ResponseEntity<Object> getDrinkById(@PathVariable("drinkId") long drinkId) {
        Optional<CDrink> drinkFound = pDrinkRepository.findById(drinkId);
        if(drinkFound.isPresent()) {
            return new ResponseEntity<>(drinkFound, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

	@PostMapping("/create")
    public ResponseEntity<Object> createDrink(@RequestBody CDrink newDrink) {
        try {
            CDrink drink = new CDrink();
			drink.setMaNuocUong(newDrink.getMaNuocUong());
			drink.setTenNuocUong(newDrink.getTenNuocUong());
			drink.setDonGia(newDrink.getDonGia());
			drink.setNgayTao(new Date());
			drink.setNgayCapNhat(null);
            return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Failed to Create Drink: " + e.getCause().getCause().getMessage());
        }
    }

	@PutMapping("/update/{drinkId}")
    public ResponseEntity<Object> updateDrink(@PathVariable("drinkId") Long drinkId, @RequestBody CDrink upDrink) {
        try {
            Optional<CDrink> drinkFound = pDrinkRepository.findById(drinkId);
			if(drinkFound.isPresent()) {
				CDrink newDrink = drinkFound.get();
				newDrink.setMaNuocUong(upDrink.getMaNuocUong());
				newDrink.setTenNuocUong(upDrink.getTenNuocUong());
				newDrink.setDonGia(upDrink.getDonGia());
				newDrink.setNgayCapNhat(new Date());
				return new ResponseEntity<>(pDrinkRepository.save(newDrink), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Failed to Update Drink: " + e.getCause().getCause().getMessage());
        }
    }

	@DeleteMapping("/delete/{drinkId}")
    public ResponseEntity<Object> deleteDrink(@PathVariable long drinkId) {
        Optional<CDrink> drinkFound = pDrinkRepository.findById(drinkId);
        if(drinkFound.isPresent()) {
            pDrinkRepository.deleteById(drinkId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
