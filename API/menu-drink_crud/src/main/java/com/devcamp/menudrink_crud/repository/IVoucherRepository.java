package com.devcamp.menudrink_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink_crud.model.CVoucher;


public interface IVoucherRepository extends JpaRepository<CVoucher, Long>{
    
}
