package com.devcamp.menudrink_crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink_crud.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
