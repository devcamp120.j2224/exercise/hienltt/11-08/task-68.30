package com.devcamp.menudrink_crud.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink_crud.model.COrder;
import com.devcamp.menudrink_crud.model.CUser;


public interface IUserRepository extends JpaRepository<CUser, Long>{
    CUser findById(long id);
}
